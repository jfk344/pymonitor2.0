### pyMonitor ###

## Information: ##
pyMonitor is a python application to monitor network devices. Its build inside a docker-compose which includes:
    
    - Django
    - Celery
    - Redis
    - Postgres DB 

## Instalation: ##

To install pyMonitor proceeded as descriped:

Requirements:

    - Docker / Docker-Compose

Download:

`$ git clone https://gitlab.com/jfk344/pymonitor2.0.git`

Start: 

```
$ cd /pymonitor2.0/pymonitor
$ docker-compose up

Result:
Starting postgres_pyMonitor   ... done
Starting redis_pyMonitor    ... done
Starting celeryBeat_pyMonitor ... done
Starting django_pyMonitor     ... done
Recreating celeryWorker_pyMonitor ... done
```

Stop:
```
$ cd /pymonitor2.0/pymonitor
$ docker-compose down

or 

CTRL + C
```

## Basic Configuration ##

Start by using `$ docker-compose up`

Now migrate the changes: `$ docker exec -it celeryWorker_pyMonitor python manage.py migrate
` 

Restart: `$ docker-compose down` followed by `docker-compose up`
1. Create Superuser:

    `$ docker exec -it celeryWorker_pyMonitor python manage.py createsuperuser`

    This user is used to login at the administration site!

To administrate: `http://X.X.X.X:8000/admin`

2. ADD a Telegram Bot:
    
    - Name: `Default` leave at default when only using one
    - Enter the Telegram Bot Token and and Chatid

3. ADD Hosts:

     - `Timeout` how many secounds the ping waits before declaring host as `Pending`
     - `MaxDownCount` how many intervals pass by befor the host jumps form `Pending` to `Down`

4. Add a Periodic Check Task:
    
    - In Periodic Tasks create a new
    - `Task (registered)` choose `monitoring.tasks.checkAll`
    - `Interval Schedule` choose how often the Hosts should be Checked
    - remove `one-off Task` checkbox

5. Create a Default Setting: 

    - you can leave all at default values

Your Telegram-Bot should send you Notifications now.

## Write to InfluxDB Configuration ## 

1. Create a InfluxDB 
    - leave `Name` at `Default`
    - Enter the other InfluxDB Parameters.

2. Create a periodic `Write-Task`
    - In Periodic Tasks create a new
    - `Task (registered)` choose `monitoring.tasks.writeAll`
    - `Interval Schedule` choose how often the Hosts should be Checked `5 min`
    - remove `one-off Task` checkbox

Now after the selected Interval you should be able to see Data in your InfluxDB

## Task Parameters ##

You can if you want different a Secound Telegram or InfluxDB define them in the Task.

`writeAll(influxDBName="Default", settingsName="Default")`

By default it will choose the Default ones. Which when you created them by the are named by default as `Default`

To choos a other one you need go to your Tasks Arguments and add: `[influxDBName="myOtherInfluxDB", settingsName="myOtherTelegram"]` 

**! Make sure to name your Telegram or Influx in the name field like the argument !**