import { createRouter, createWebHistory } from 'vue-router'
import StatusTable from '../views/StatusTable.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'statusTable',
      component: StatusTable
    },
  ]
})

export default router
