from celery import shared_task
from django.conf import settings
import logging as log
import os
import pytz
from icmplib import ping as pyping
from icmplib import resolve
from icmplib import NameLookupError, ICMPSocketError
import ipaddress
import http.client, urllib
from tcp_latency import measure_latency
from string import Template
#from . import constVars

if settings.DEBUG:
    log.basicConfig(level=os.environ.get("LOGLEVEL", "DEBUG"))
else:
    log.basicConfig(level=os.environ.get("LOGLEVEL", "INFO"))

#log.basicConfig(filename="logfile.txt", level=log.DEBUG)


from monitoring.models import (
    Settings,
    Telegram,
    InfluxDB,
    Hosts,
    Pushover,
    TcpDestination,
    TcpStatus,
    PingDestination,
    PingStatus,
    HaDestination,
    HaStatus,
)

#from ping3 import ping as pyping
import requests
from requests import get
from influxdb_client import InfluxDBClient, Point, WritePrecision
from influxdb_client.client.write_api import SYNCHRONOUS
from django.utils import timezone
from datetime import datetime

class status:
    ok = "OK"
    pending = "PENDING"
    sslError = "SSLERROR"
    serverError = "SERVERERROR"
    down = "DOWN"
    dnsError = "DNS-Error"
    untracked = "Untracked" 

def sub(host, input):
    output = Template(input)
    output = output.substitute(
            address=f"{host.address}",
            name=f"{host.name}"
        )
    return output

def getNotificationContent(host):
    pingStatus = ""
    if host.ping.exists():
        pingStates = PingStatus.objects.filter(host=host)
        for pingState in pingStates:
            if pingState.isAlive == True:
                pingStatus += f"🟢 Ping: {pingState.name}\n"
            elif pingState.isAlive == False:
                pingStatus += f"🔴 Ping: {pingState.name}\n"
            else:
                pingStatus += f"⚫️ Ping: {pingState.name}\n"

            pingState.previousIsAlive = pingState.isAlive
            pingState.save()

    tcpStatus = ""
    if host.tcp.exists():
        tcpStates = TcpStatus.objects.filter(host=host)
        for tcpState in tcpStates:
            if tcpState.isAlive == True:
                tcpStatus += f"🟢 HTTP: {tcpState.name}\n"
            elif tcpState.isAlive == False:
                tcpStatus += f"🔴 HTTP: {tcpState.name}\n"
            else:
                tcpStatus += f"⚫️ HTTP: {tcpState.name}\n"

            tcpState.previousIsAlive = tcpState.isAlive
            tcpState.save()

    haStatus = ""
    if host.ha.exists():
        haStates = HaStatus.objects.filter(host=host)
        for haState in haStates:
            if haState.isAlive ==True:
                haStatus += f"🟢 HomeAssistant: {haState.name}\n"
            elif haState.isAlive == False:
                haStatus += f"🔴 HomeAssistant: {haState.name}\n"
            else:
                haStatus += f"⚫️ HomeAssistant: {haState.name}\n"

            haState.previousIsAlive = haState.isAlive
            haState.save()

    content = f"""Host \"{host.name}\" Status Update: \n{pingStatus} \n{tcpStatus} \n{haStatus}"""
    return content

def evalNotify(host):
    pingStates = PingStatus.objects.filter(host=host)
    tcpStates = TcpStatus.objects.filter(host=host)
    haStates = HaStatus.objects.filter(host=host)

    changeDetected = False

    for pingState in pingStates:
        if pingState.isAlive != pingState.previousIsAlive:
            changeDetected = True
    for tcpState in tcpStates:
        if tcpState.isAlive != tcpState.previousIsAlive:
            changeDetected = True
    for haState in haStates:
        if haState.isAlive != haState.previousIsAlive:
            changeDetected = True

    return changeDetected


def TelegramSendMessage(message, telegram):
    try:
        send_text = f"https://api.telegram.org/bot{telegram.token}/sendMessage?chat_id={telegram.chatId}&parse_mode=Markdown&text={message}"
        response = requests.get(send_text)
        if response.status_code != 200:
            log.error(f"Telegram Resp: {response}")
        return response.json()
    except Exception as e:
        log.error(f"Telegram Exception: {e}")


def PushoverSendMessage(message, pushover):
    try:
        conn = http.client.HTTPSConnection("api.pushover.net:443")
        conn.request("POST", "/1/messages.json",
            urllib.parse.urlencode({
                "token": f"{pushover.token}",
                "user": f"{pushover.userId}",
                "message": f"{message}",
            }), { "Content-type": "application/x-www-form-urlencoded" })
        response = conn.getresponse()
    except Exception as e:
        log.error(f"Pushover Exception: {e}")
        
class Influx:
    def __init__(self, bucket, org, url, token):
        self.bucket = bucket
        self.org = org
        self.url = url
        self.token = token
        self.interval = 300

        self.client = InfluxDBClient(url=self.url, token=self.token)
        self.write_api = self.client.write_api(write_options=SYNCHRONOUS)

    def write(self, name, field_name, value, group):
        try:
            point = Point(name)\
                .field(field_name, value)\
                .tag('group', group)\
                .time(datetime.utcnow(), WritePrecision.NS)
            result = self.write_api.write(self.bucket, self.org, point)
            return result
        except Exception as e:
            log.error(f"influx Write Exception {e}")
            return e

    def queryLast(self, measurement):
        try:
            query = f"""from(bucket: "{self.bucket}")
                |> range(start: -{self.interval + 120}s, stop: now())
                |> filter(fn: (r) => r["_measurement"] == "{measurement}")
                |> filter(fn: (r) => r["_field"] == "status_code")
                |> yield(name: "last")"""
            tables = self.client.query_api().query(query, org=self.org)
            results = []
            for table in tables:
                for record in table.records:
                    results.append((record.get_value(), record.get_field()))
            if len(results) > 0:
                result = str(results[len(results)-1][0])
            else:
                result = "INIT"
            log.debug(f"Query Last for {measurement} returned: {result}")
            return result
        except Exception as e:
            log.error(f"Influx Query Exception: {e}")
            return "INIT"

    def writeStatusCode(self, name, status, fieldName, group="Default"):
        response = self.write(name = name, field_name = "status_code", value=str(status), group=group)
        log.debug(f"write Status_Code Response {response}")

    def writeLatency(self, name, delay, group="Default"):
        response = self.write(name = name, field_name = "latency", value=float(delay), group=group)
        log.debug(f"write Latency Response {response}")

def Tcp(host):
    response = 0
    try:
        TcpDestinations = host.tcp.all()
        for TcpDestination in TcpDestinations:
            response = measure_latency(host=f"{sub(host=host, input=TcpDestination.destination)}", port=TcpDestination.port, runs=TcpDestination.runs, timeout=TcpDestination.timeout)
            sum = 0
            count = 0
            for element in response:
                if element != None:
                    sum = sum + element
                    count += 1
            if sum != 0 and count != 0:
                mean_response = round(sum/count, 2)
            else:
                mean_response = 0
            if type(mean_response) == float and mean_response > 0:
                TcpStatus.objects.update_or_create(
                    name=sub(host=host, input=TcpDestination.name),
                    defaults={
                        'host': host,
                        'isAlive': True,
                        'latency': mean_response,
                    }
                )
            else:
                raise Exception

    except Exception as e:
        log.error(f"TCP: Exception {e}")
        TcpStatus.objects.update_or_create(
            name=sub(host=host, input=TcpDestination.name),
            defaults={
                'host': host,
                'isAlive': False,
                'latency': 0,
            }
        )


def Ping(host):
    pingDestinations = host.ping.all()
    for pingDestination in pingDestinations:
        try:
            try:
                destination = sub(host=host, input=pingDestination.destination)
                ipaddress.ip_address(destination)
                destinationIp = destination
            except ValueError:
                try: 
                    destinationIp = str(resolve(destination)[0])
                except NameLookupError:
                    raise Exception
            try:
                ping_result = pyping(
                        destinationIp,
                        count=pingDestination.count,
                        interval=pingDestination.interval,
                        timeout=pingDestination.timeout
                    )
                if ping_result.is_alive:
                    PingStatus.objects.update_or_create(
                    name=sub(host=host, input=pingDestination.name),
                    defaults={
                        'host': host,
                        'isAlive': True,
                        'latency': round(ping_result.avg_rtt, 1),
                        'packet_loss': round(ping_result.packet_loss * 100, 0)
                        }
                    )            
                else:
                    raise Exception

            except NameLookupError:
                log.error(f"NameLookupError")
                raise Exception

            except ICMPSocketError:
                log.error(f"SocketAddressError")
                raise Exception

        except Exception as e:
            log.error(f"ICMP Exception: {e}")
            PingStatus.objects.update_or_create(
            name=sub(host=host, input=pingDestination.name),
            defaults={
                'host': host,
                'isAlive': False,
                'latency': 0,
                'packet_loss': 100,
                }
            )  


def Ha(host):
    try:
        haDestinations = host.ha.all()
        for haDestination in haDestinations:
            url = f"{haDestination.homeassistant.address}/api/states/{sub(host=host, input=haDestination.haEntityId)}"
            headers = {
                "Authorization": f"Bearer {haDestination.homeassistant.apiKey}",
                "content-type": "application/json",
            }
            response = get(url, headers=headers)
            if response.status_code == 401:
                log.error(f"Homeassistant not Authorized")
            if response.status_code == 200:
                haEntityState = response.json()['state']
                if haEntityState == "unavailable" or haEntityState == "disconnected" or haEntityState == "Unavailable":
                    raise Exception
                else:
                    HaStatus.objects.update_or_create(
                    name=sub(host=host, input=haDestination.name),
                    defaults={
                        'host': host,
                        'isAlive': True,
                        }
                    )
    except Exception as e:
        log.error(f"HA Exception: {e}")
        HaStatus.objects.update_or_create(
        name=sub(host=host, input=haDestination.name),
        defaults={
            'host': host,
            'isAlive': False,
            }
        )

def notify(host, content=None):
    try:
        if content == None:
            content = getNotificationContent(host=host)

        if host.telegram.exists():
            telegrams = host.telegram.all()
            for telegram in telegrams:
                TelegramSendMessage(message=content, telegram=telegram)
        if host.pushover.exists():
            pushovers = host.pushover.all()
            for pushover in pushovers:
                PushoverSendMessage(message=content, pushover=pushover)

    except Exception as e:
        log.error(f"Error Notify: {e}")

@shared_task
def writeAll(influxDBName="Default", settingsName="Default"):

    try: 
        influxdbModel = InfluxDB.objects.get(name=influxDBName)

        influxDB_token = influxdbModel.influxdb_token
        influxDB_org = influxdbModel.influxdb_org
        influxDB_url = influxdbModel.influxdb_url
        influxDB_bucket = influxdbModel.influxdb_bucket

        influxdb = Influx(bucket=influxDB_bucket, org=influxDB_org, url=influxDB_url, token=influxDB_token)

    except InfluxDB.DoesNotExist:
        log.error(f"Write Task without InfluxDB defined!")
        influxdb = None

    hosts = Hosts.objects.all()

    if influxdb != None:
        for host in hosts:
            if host.writeToDB:
                if host.checkPing:
                    influxdb.writeLatency(name=host.name, delay=host.latestLatency, group=host.group)
                    influxdb.writeStatusCode(name=host.name, status=host.latestPingStatus, fieldName="pingStatus", group=host.group)
                if host.checkHttp:
                    influxdb.writeStatusCode(name=host.name, status=host.latestHttpStatus, fieldName="httpStatus", group=host.group)

@shared_task
def checkAll(settingsName="Default"):
    hosts = Hosts.objects.all()
    if hosts:
        for host in hosts:

            if(host.ping.exists()):
                Ping(host)

            if (host.ha.exists()):
                Ha(host)

            if (host.tcp.exists()):
                Tcp(host)

            if evalNotify(host=host):
                notify(host=host)

            now = timezone.localtime().strftime("%Y-%m-%d %H:%M:%S")
            host.lastChecked = now
            host.save()

            