# Generated by Django 3.2.6 on 2021-09-05 16:09

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('monitoring', '0028_auto_20210905_1742'),
    ]

    operations = [
        migrations.AddField(
            model_name='hosts',
            name='telegramName',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='monitoring.telegram'),
        ),
    ]
