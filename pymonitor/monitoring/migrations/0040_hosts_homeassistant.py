# Generated by Django 3.2.6 on 2021-09-24 08:05

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('monitoring', '0039_auto_20210924_0957'),
    ]

    operations = [
        migrations.AddField(
            model_name='hosts',
            name='homeassistant',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='monitoring.homeassistant'),
        ),
    ]
