# Generated by Django 3.2.6 on 2021-08-11 12:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('monitoring', '0002_auto_20210811_0926'),
    ]

    operations = [
        migrations.CreateModel(
            name='HttpHosts',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('ip', models.CharField(max_length=200)),
                ('name', models.CharField(max_length=200)),
                ('group', models.CharField(max_length=200)),
                ('timeout', models.IntegerField(default=2)),
                ('latestStatusCode', models.CharField(max_length=200)),
                ('currentDownCount', models.IntegerField(default=0)),
                ('previousState', models.CharField(max_length=200)),
                ('lastDBState', models.CharField(max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='InfluxDB',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('influxdb_token', models.CharField(max_length=200)),
                ('influxdb_org', models.CharField(max_length=200)),
                ('influxdb_url', models.CharField(max_length=200)),
                ('influxdb_bucket', models.CharField(max_length=200)),
                ('influxdb_writeInterval', models.IntegerField(default=300)),
            ],
        ),
        migrations.CreateModel(
            name='Settings',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('interval', models.IntegerField(default=10)),
                ('debug', models.BooleanField(default=False)),
                ('defaultPingTimeout', models.IntegerField(default=2)),
                ('defaultMaxDownCount', models.IntegerField(default=2)),
            ],
        ),
        migrations.CreateModel(
            name='Telegram',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('telegram_token', models.CharField(max_length=200)),
                ('telegram_chatid', models.CharField(max_length=200)),
            ],
        ),
    ]
