# Generated by Django 3.2.6 on 2021-08-15 16:34

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('monitoring', '0018_alter_hosts_lastchecked'),
    ]

    operations = [
        migrations.CreateModel(
            name='HostGroups',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(default='Default', max_length=200)),
            ],
            options={
                'verbose_name': 'InfluxDB',
                'verbose_name_plural': 'InfluxDBs',
            },
        ),
        migrations.AlterField(
            model_name='hosts',
            name='group',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='monitoring.hostgroups'),
        ),
    ]
