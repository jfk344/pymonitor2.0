# Generated by Django 3.2.6 on 2021-08-15 16:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('monitoring', '0017_hosts_lastchecked'),
    ]

    operations = [
        migrations.AlterField(
            model_name='hosts',
            name='lastChecked',
            field=models.DateTimeField(auto_now=True),
        ),
    ]
