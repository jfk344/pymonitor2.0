# Generated by Django 3.2.6 on 2021-09-27 20:13

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('monitoring', '0051_auto_20210927_2208'),
    ]

    operations = [
        migrations.CreateModel(
            name='TcpStatus',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uuid', models.UUIDField(default=uuid.uuid4, unique=True)),
                ('name', models.CharField(max_length=200)),
                ('isAlive', models.BooleanField(null=True)),
                ('latency', models.FloatField(null=True)),
            ],
            options={
                'verbose_name': 'TCP Status',
                'verbose_name_plural': 'TCP States',
            },
        ),
    ]
