from django.contrib import admin


from .models import (
        Homeassistant,
        Hosts,
        HostGroups,
        Settings,
        Telegram,
        InfluxDB,
        Pushover,
        TcpDestination,
        TcpStatus,
        PingDestination,
        PingStatus,
        HaDestination,
        HaStatus,
)

admin.site.register(HostGroups)
admin.site.register(Hosts)
admin.site.register(Settings)
admin.site.register(Homeassistant)
admin.site.register(Telegram)
admin.site.register(InfluxDB)
admin.site.register(Pushover)
admin.site.register(TcpDestination)
admin.site.register(TcpStatus)
admin.site.register(PingDestination)
admin.site.register(PingStatus)
admin.site.register(HaDestination)
admin.site.register(HaStatus)