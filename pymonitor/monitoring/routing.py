from django.urls import path

from .consumers import WSConsumer
from .consumers import singleHostConsumer

ws_urlpatterns = [
    path('ws/hostsData/', WSConsumer.as_asgi()),
    path('ws/hostData/', singleHostConsumer.as_asgi())
]