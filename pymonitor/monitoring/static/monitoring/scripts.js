function filterFunction() {
    // Declare variables
    var input, filter, table, tr, td, i, txtValue;
    input = document.getElementById("filterInput");
    filter = input.value.toUpperCase();
    table = document.getElementById("statusTable");
    tr = table.getElementsByTagName("tr");
  
    // Loop through all table rows, and hide those who don't match the search query
    for (i = 0; i < tr.length; i++) {
      td = tr[i].getElementsByTagName("td")[0];
      if (td) {
        txtValue = td.textContent || td.innerText;
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
          tr[i].style.display = "";
        } else {
          tr[i].style.display = "none";
        }
      }
    }
}

function startStatusTableWs() {
    var loc = window.location
    var wsStart = 'ws://'
    if (loc.protocol == 'https:'){
    wsStart = 'wss://'
    }
    console.log(wsStart + loc.host + '/ws/hostsData/')
    var socket = new WebSocket(wsStart + loc.host + '/ws/hostsData/');

    socket.onmessage = function(event){
    var hosts = JSON.parse(event.data)
    console.log(hosts)
    var statusTable = document.getElementById("statusTable")
    var statusTableBody = document.getElementById("statusTableBody")
    var neededRows = hosts.length -  (statusTable.rows.length - 1)
    if (neededRows > 0){
        console.log("Adding new rows")
        for (var r = 0; r < neededRows; r++){
            var newRow = statusTableBody.insertRow()
            for (var c = 0; c < 6; c++ ){
                var newCell = newRow.insertCell()
                //newCell.innerHTML =  hosts[i].name
            }
        }
    }
    if (neededRows < 0){ //need more rows
        console.log("Deleting rows")
        for (var r = 0; r < neededRows; r++){
            var newRow = statusTable.deleteRow()
        }
    }

    var online = "🟢"
    var pending = "🟠"
    var serverError = "🟣"
    var sslError = "🔓"
    var offline = "🔴"
    var untracked = "⚫️"
    var dnsError = "🧐"

    var groupIconAP = ""
    var groupIconInet = "🌐"

    var statusOk = "OK"
    var statusPending = "PENDING"
    var statusSslError = "SSLERROR"
    var statusServerError = "SERVERERROR"
    var statusDown = "DOWN"
    var StatusDnsError = "DNS-Error"

    console.log("Using existing Table")
    for (var r = 0; r < hosts.length; r += 1) {
        if (hosts[r].hasOwnProperty('tcpStatus')){
            latestPingStatus = ""
            for (var i = 0; i < hosts[r].pingStatus.length; i += 1){
                if (hosts[r].pingStatus[i].isAlive == true){
                    pingStatusIcon = online
                }
                else{
                    pingStatusIcon = offline
                }
                latestPingStatus = latestPingStatus + hosts[r].pingStatus[i].name + " " + pingStatusIcon + "<br>"
            }
        }  else {
            latestPingStatus = ""
        }

        if (hosts[r].hasOwnProperty('tcpStatus')){
            latestTcpStatus = ""
            for (var i = 0; i < hosts[r].tcpStatus.length; i += 1){
                if (hosts[r].tcpStatus[i].isAlive == true){
                    tcpStatusIcon = online
                }
                else{
                    tcpStatusIcon = offline
                }
                latestTcpStatus = latestTcpStatus + hosts[r].tcpStatus[i].name + " " + tcpStatusIcon + "<br>"
            }
        } else {
            latestTcpStatus = ""
        }

        if (hosts[r].hasOwnProperty('haStatus')){
            latestHaStatus = ""
            for (var i = 0; i < hosts[r].haStatus.length; i += 1){
                if (hosts[r].haStatus[i].isAlive == true){
                    haStatusIcon = online
                }
                else{
                    haStatusIcon = offline
                }
                latestHaStatus = latestHaStatus + hosts[r].haStatus[i].name + " " + haStatusIcon + "<br>"
            }
        } else {
            latestHaStatus = ""
        }


        statusTableBody.rows[r].cells[0].innerHTML = hosts[r].name;
        statusTableBody.rows[r].cells[1].innerHTML = latestPingStatus;
        statusTableBody.rows[r].cells[2].innerHTML = latestTcpStatus;
        statusTableBody.rows[r].cells[3].innerHTML = latestHaStatus;
        statusTableBody.rows[r].cells[4].innerHTML = hosts[r].group;
        statusTableBody.rows[r].cells[5].innerHTML = "<a href='/monitoring/host-edit/"+hosts[r].uuid+"'>✏️</a><a href='/monitoring/host/"+hosts[r].uuid+"'>🔎</a>";
    }
}

  socket.onclose = function(e) {
      //console.error('Chat socket closed unexpectedly');
      console.log('Socket is closed. Reconnect will be attempted in 1 second.', e.reason);
      var notification = new Notification("Websocket Closed", {body: "Data wont be updated. Retry every 1 secound."});
      setTimeout(function() {notification.close()}, 1000);
      setTimeout(function() {
          startStatusTableWs();
      }, 1000);
  };
};

function startSingleHostWs() {
    var loc = window.location
    var wsStart = 'ws://'
    if (loc.protocol == 'https:'){
      wsStart = 'wss://'
    }
    var uuid = window.location.pathname.split('/')[3];
    console.log(wsStart + loc.host + '/ws/hostData/')
    var socket = new WebSocket(wsStart + loc.host + '/ws/hostData/');

    socket.onopen = function(event) {
        socket.send(JSON.stringify({'uuid':uuid}));
        setInterval(function(){ socket.send(JSON.stringify({'uuid':uuid})); }, 10000);      
    };
    
    socket.onmessage = function(event){
        var host = JSON.parse(event.data)
        console.log(host)

        document.getElementById("h1").innerHTML = host.name;

        document.getElementById("uuid").innerHTML = host.uuid;
        document.getElementById("head").innerHTML = host.name;
        document.getElementById("pingDestination").innerHTML = host.pingDestination;
        document.getElementById("httpDestination").innerHTML = host.httpDestination;
        document.getElementById("group").innerHTML = host.group;
        document.getElementById("writeToDB").innerHTML = host.writeToDB;
        document.getElementById("sendWhenPending").innerHTML = host.sendWhenPending;
        document.getElementById("checkPing").innerHTML = host.checkPing;
        document.getElementById("pingDestination").innerHTML = host.pingDestination;
        document.getElementById("checkHttp").innerHTML = host.checkHttp;
        document.getElementById("checkSSL").innerHTML = host.checkSSL;
        document.getElementById("maxDownCount").innerHTML = host.maxDownCount;
        document.getElementById("timeout").innerHTML = host.timeout;
        document.getElementById("latestPingStatus").innerHTML = host.latestPingStatus;
        document.getElementById("latestHttpStatus").innerHTML = host.latestHttpStatus;
        document.getElementById("currentPingDownCount").innerHTML = host.currentPingDownCount;
        document.getElementById("currentHttpDownCount").innerHTML = host.currentHttpDownCount;
        document.getElementById("latestLatency").innerHTML = host.latestLatency + ' ms';
        document.getElementById("lastChecked").innerHTML = host.lastChecked;
        document.getElementById("lastPingDown").innerHTML = host.lastPingDown;
        
        //BUTTONS
        document.getElementById("deleteHostForm").action = "/monitoring/host-delete/"+host.uuid;

        socket.onclose = function(e) {
            //console.error('Chat socket closed unexpectedly');
            console.log('Socket is closed. Reconnect will be attempted in 1 second.', e.reason);
            var notification = new Notification("Websocket Closed", {body: "Data wont be updated. Retry every 1 secound."});
            setTimeout(function() {notification.close()}, 1000);
            setTimeout(function() {
                startStatusTableWs();
            }, 1000);
        };
    };
}