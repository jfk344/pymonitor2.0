from channels.generic.websocket import WebsocketConsumer

import asyncio
#from asgiref.sync import sync_to_async

from channels.db import database_sync_to_async

from channels.generic.websocket import AsyncJsonWebsocketConsumer

import json
from random import randint
from time import sleep

from monitoring.models import (
    Hosts,
    HostGroups,
)


class WSConsumer(AsyncJsonWebsocketConsumer):

    @database_sync_to_async
    def getHosts(self):
        return Hosts.getAllHostsList()

    async def connect(self):
        await self.accept()

        while True:
            hostsList = []

            hosts = await self.getHosts()

            await self.send_json(hosts)

            await asyncio.sleep(5)

    async def disconnect(self, close_code):
        print("Websocket Disconnected")


class singleHostConsumer(AsyncJsonWebsocketConsumer):
    @database_sync_to_async
    def getHost(self, uuid):
        return Hosts.getSingleHostList(uuid)

    async def connect(self):
        await self.accept()

    async def receive(self, text_data):
        message = json.loads(text_data)
        uuid = message.get('uuid', None)
        if uuid != None:
            host = await self.getHost(uuid)
            await self.send_json(host)
        else:
            print("Error no uuid Found")

    async def disconnect(self, close_code):
        print("Websocket Disconnected")