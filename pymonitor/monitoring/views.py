from django.shortcuts import render
from django.http import HttpResponse
from django.http import HttpResponseRedirect

#Rest-Framework
from rest_framework import viewsets, permissions
from rest_framework.authentication import SessionAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response


import uuid

from monitoring.models import (
    Settings,
    Telegram,
    InfluxDB,
    Hosts,
    HostGroups,
)

from monitoring.serializers import (
    HostSerializer
)

###########################################################################################################################################
################################################################## VUE ####################################################################
###########################################################################################################################################
def vueApp(request):
    return render(request, 'index.html')

###########################################################################################################################################
################################################################# MODELS ##################################################################
###########################################################################################################################################
class Hosts(viewsets.ModelViewSet):
    permission_classes = [permissions.AllowAny]
    queryset = Hosts.objects.all()
    serializer_class = HostSerializer
    http_method_names = ['get']
    filter_fields = ['uuid']
