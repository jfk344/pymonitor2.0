from django import forms

import uuid

class HostsForm(forms.Form):
    #Basic
    #uuid = forms.CharField(max_length=200)
    name = forms.CharField(max_length=200, required=True)
    group = forms.CharField(max_length=200, required=True)
    #Monitoring config
    checkPing = forms.BooleanField(required=False)
    pingDestination = forms.CharField(max_length=200, required=False)

    checkHttp = forms.BooleanField(required=False)
    httpDestination = forms.CharField(max_length=200, required=False)
    checkSSL = forms.BooleanField(required=False)

    #InfluxDB
    writeToDB = forms.BooleanField(required=False)

    #Telegram
    notifyTelegram = forms.BooleanField(required=False)
    telegramName = forms.CharField(max_length=200, required=False)

    #Notify Logic
    notifyWhenPending = forms.BooleanField(required=False)
    notifyWhenDown= forms.BooleanField(required=False) 

    maxDownCount = forms.IntegerField(required=True)
    timeout = forms.IntegerField(required=True)

class HostGroupsForm(forms.Form):
    name = forms.CharField(max_length=200, required=True)