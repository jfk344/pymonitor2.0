from email.mime import base
from posixpath import basename
from django.urls import re_path, path

from monitoring import views
from monitoring import apis

# REST
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'hosts', views.Hosts, basename="hosts")

apipatterns = [
]

urlpatterns = [
    re_path(r'^', views.vueApp),
]
