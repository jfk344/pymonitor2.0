from django.db import models
from datetime import datetime

import uuid

class Settings(models.Model):
    name = models.CharField(max_length=200, default="Default")
    logLevel = models.BooleanField(null=False, default=False)

    class Meta:
        verbose_name = 'Setting'
        verbose_name_plural = 'Settings'

    def __str__(self):
        return self.name
class Homeassistant(models.Model):
    uuid = models.UUIDField(default=uuid.uuid4, unique=True)
    name = models.CharField(max_length=200, default="My Homeassistant")
    address = models.CharField(max_length=200, help_text="https://myHomeassistant.com")
    apiKey = models.CharField(max_length=200, help_text="https://developers.home-assistant.io/docs/api/rest/")
    class Meta:
        verbose_name = 'Homeassistant'
        verbose_name_plural = 'Homeassistants'

    def __str__(self):
        return self.name

class Telegram(models.Model):
    name = models.CharField(max_length=200, default="Default")
    token = models.CharField(max_length=200) 
    chatId = models.CharField(max_length=200)

    class Meta:
        verbose_name = 'Telegram'
        verbose_name_plural = 'Telegrams'

    def __str__(self):
        return self.name

class Pushover(models.Model):
    name = models.CharField(max_length=200, default="Default")
    token = models.CharField(max_length=200) 
    userId = models.CharField(max_length=200)

    class Meta:
        verbose_name = 'Pushover'
        verbose_name_plural = 'Pushovers'

    def __str__(self):
        return self.name

class InfluxDB(models.Model):
    name = models.CharField(max_length=200, default="Default")
    influxdb_token = models.CharField(max_length=200)
    influxdb_org = models.CharField(max_length=200)
    influxdb_url = models.CharField(max_length=200)
    influxdb_bucket = models.CharField(max_length=200)
    influxdb_writeInterval = models.IntegerField(default=300)

    class Meta:
        verbose_name = 'InfluxDB'
        verbose_name_plural = 'InfluxDBs'

    def __str__(self):
        return self.name



class HostGroups(models.Model):
    name = models.CharField(max_length=200, default="Default")

    class Meta:
        verbose_name = 'HostGroup'
        verbose_name_plural = 'HostGroups'

    def __str__(self):
        return self.name

class TcpDestination(models.Model):
    uuid = models.UUIDField(default=uuid.uuid4, unique=True)
    name = models.CharField(max_length=200)
    destination = models.CharField(max_length=200)
    port = models.IntegerField()
    runs = models.IntegerField(help_text="How ofthen the connection is tested")
    timeout = models.FloatField(default=2, help_text="TCP Timeout in Secounds")
    class Meta:
        verbose_name = 'TCP Destination'
        verbose_name_plural = 'TCP Destinations'

    def __str__(self):
        return self.name
class PingDestination(models.Model):
    uuid = models.UUIDField(default=uuid.uuid4, unique=True)
    name = models.CharField(max_length=200)
    destination = models.CharField(max_length=200)
    timeout = models.FloatField(default=2, help_text="TCP Timeout in Secounds")
    count = models.IntegerField(null=True)
    interval = models.FloatField(null=True)
    class Meta:
        verbose_name = 'Ping Destination'
        verbose_name_plural = 'Ping Destinations'

    def __str__(self):
        return self.name

class HaDestination(models.Model):
    uuid = models.UUIDField(default=uuid.uuid4, unique=True)
    name = models.CharField(max_length=200)
    homeassistant = models.ForeignKey(Homeassistant, on_delete=models.CASCADE, null=True)
    haEntityId = models.CharField(max_length=200, blank=True)
    class Meta:
        verbose_name = 'HA Destination'
        verbose_name_plural = 'HA Destinations'

    def __str__(self):
        return self.name
class Hosts(models.Model):
    uuid = models.UUIDField(default=uuid.uuid4, unique=True)
    name = models.CharField(max_length=200, blank=False, unique=True)
    address = models.CharField(max_length=200, blank=True, help_text="10.0.0.1 / test.local")
    group = models.ForeignKey(HostGroups, on_delete=models.CASCADE, null=True)

    writeToDB = models.BooleanField(null=False, default=True)

    telegram = models.ManyToManyField(Telegram, blank=True)

    pushover = models.ManyToManyField(Pushover, blank=True)

    notifyWhenPending = models.BooleanField(null=False, default=False)
    notifyWhenDown= models.BooleanField(null=False, default=False)

    tcp = models.ManyToManyField(TcpDestination, blank=True)
    ping = models.ManyToManyField(PingDestination, blank=True)
    ha = models.ManyToManyField(HaDestination, blank=True)
    
    lastChecked = models.CharField(max_length=200, default="Untracked")

    class Meta:
        verbose_name = 'Host'
        verbose_name_plural = 'Hosts'
        ordering = ['group']

    def __str__(self):
        return self.name

    def __str__(self):
        return self.name


    def getAllHostsList():
        hostsList = []
        hosts = Hosts.objects.all()
        for host in hosts:
                singleHost = {
                    'uuid': str(host.uuid),
                    'name': host.name,
                    'group': host.group.name,
                }
                pingStatus = PingStatus.objects.filter(host=host)
                pingStatusList = []
                for pingState in pingStatus:
                    pingStatusList.append({
                        'uuid': str(pingState.uuid),
                        'name': pingState.name,
                        'isAlive': pingState.isAlive,
                        'latency': pingState.latency,
                        'packet_loss': pingState.packet_loss,
                    })
                    singleHost['pingStatus']=pingStatusList

                tcpStatus = TcpStatus.objects.filter(host=host)
                tcpStatusList = []
                for tcpState in tcpStatus:
                    tcpStatusList.append({
                        'uuid': str(tcpState.uuid),
                        'name': tcpState.name,
                        'isAlive': tcpState.isAlive,
                        'latency': tcpState.latency,
                    })
                    singleHost['tcpStatus']=tcpStatusList

                haStatus = HaStatus.objects.filter(host=host)
                haStatusList = []
                for haState in haStatus:
                    haStatusList.append({
                        'uuid': str(haState.uuid),
                        'name': haState.name,
                        'isAlive': haState.isAlive,
                    })
                    singleHost['haStatus']=haStatusList

                hostsList.append(singleHost)
        return hostsList

    def getSingleHostList(uuid):
        host = Hosts.objects.filter(uuid=uuid)[0]
        host = {
            'uuid': str(host.uuid),
            'name': host.name,
            'group': host.group.name,
            'writeToDB': host.writeToDB,
        }
        return host


class TcpStatus(models.Model):
    uuid = models.UUIDField(default=uuid.uuid4, unique=True)
    name = models.CharField(max_length=200)
    host = models.ForeignKey(Hosts, on_delete=models.CASCADE, null=True)
    isAlive = models.BooleanField(null=True)
    previousIsAlive = models.BooleanField(null=True)
    latency = models.FloatField(null=True)
    class Meta:
        verbose_name = 'TCP Status'
        verbose_name_plural = 'TCP States'

    def __str__(self):
        return self.name
class PingStatus(models.Model):
    uuid = models.UUIDField(default=uuid.uuid4, unique=True)
    name = models.CharField(max_length=200)
    host = models.ForeignKey(Hosts, on_delete=models.CASCADE, null=True)
    isAlive = models.BooleanField(null=True)
    previousIsAlive = models.BooleanField(null=True)
    latency = models.FloatField(null=True)
    packet_loss = models.FloatField(null=True)
    class Meta:
        verbose_name = 'Ping Status'
        verbose_name_plural = 'Ping States'

    def __str__(self):
        return self.name

class HaStatus(models.Model):
    uuid = models.UUIDField(default=uuid.uuid4, unique=True)
    name = models.CharField(max_length=200)
    host = models.ForeignKey(Hosts, on_delete=models.CASCADE, null=True)
    isAlive = models.BooleanField(null=True)
    previousIsAlive = models.BooleanField(null=True)
    class Meta:
        verbose_name = 'HA Status'
        verbose_name_plural = 'HA States'

    def __str__(self):
        return self.name