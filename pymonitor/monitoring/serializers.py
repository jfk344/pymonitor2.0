from rest_framework import serializers

from monitoring.models import (
    Hosts
)

class HostSerializer(serializers.ModelSerializer):
    class Meta:
        model = Hosts
        fields = ['uuid', 'name', 'group']
