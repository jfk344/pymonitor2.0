from django.contrib import admin
from django.urls import path, re_path
from django.urls.conf import include

from rest_framework import routers

from monitoring.urls import urlpatterns as monitoring_urls
from monitoring.urls import router as monitoring_router
from monitoring.urls import apipatterns as monitoring_apis

api_urls = [] + monitoring_apis 

router = routers.DefaultRouter()
router.registry.extend(monitoring_router.registry)

admin.site.site_title = 'PyMonitor'
admin.site.site_header = 'PyMonitor'
admin.site.index_title = 'PyMonitor'

urlpatterns = [
    path('admin/', admin.site.urls),
    path("api/", include((router.urls + api_urls, "api")), name="api"),
] + monitoring_urls
